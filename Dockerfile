# FROM node:10 as repo
# #ARG SSH_KEY

# # RUN mkdir /root/.ssh && \
# # chmod 700 /root/.ssh && \
# # echo "${SSH_KEY}" >> /root/.ssh/id_rsa && \
# # chmod 600 /root/.ssh/id_rsa && \
# # ssh-keyscan github.com >> /root/.ssh/known_hosts

# WORKDIR /app
# COPY package.json package-lock.json .babelrc .flowconfig ./
# COPY src/ src/
# RUN npm ci --unsafe-perm

FROM node:10

RUN useradd -m app

WORKDIR /app
RUN chown app:app /app
COPY --chown=app --from=repo /app/ .

USER app

CMD ["npm", "start"]
