// @flow strict

// flowlint untyped-import:off
import restify from 'restify';
// flowlint untyped-import:warn
import logger from './logger';

function respond(req, res, next) {
  res.send('hello world!');
  next();
}

const server = restify.createServer();
server.get('/', respond);

server.listen(process.env.PORT, function() {
  logger.info(`${server.name} listening at ${server.url}`);
});
