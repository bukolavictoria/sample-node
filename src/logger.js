// @flow strict
// flowlint untyped-import:off
import { createLogger, format, transports } from 'winston';
const { cli } = format;
// flowlint untyped-import:warn

const logger = createLogger({
  level: 'info',
  format: cli(),
  transports: [new transports.Console()],
});

export default logger;
